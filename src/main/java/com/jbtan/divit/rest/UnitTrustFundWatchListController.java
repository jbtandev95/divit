package com.jbtan.divit.rest;

import com.jbtan.divit.constant.WebConstants;
import com.jbtan.divit.domain.UnitTrustFundWatchList;
import com.jbtan.divit.dto.UnitTrustFundWatchListDTO;
import com.jbtan.divit.service.UnitTrustFundWatchListService;
import com.jbtan.divit.useCase.UnitTrustFundWatchListUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(WebConstants.API_PREFIX + "watchlist")
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundWatchListController {

    private final UnitTrustFundWatchListUseCase unitTrustFundWatchListUseCase;

    @GetMapping
    public ResponseEntity<ApiRes<List<UnitTrustFundWatchList>>> getWatchList(Pageable pageable) {

        return ResponseEntity.ok(ApiRes.withSuccess(unitTrustFundWatchListUseCase.getWatchListByUserId(pageable).getContent()));
    }

    @PostMapping
    public ResponseEntity<ApiRes<String>> addToWatchList(@RequestBody UnitTrustFundWatchListDTO request) {

        unitTrustFundWatchListUseCase.addToWatchList(request);

        return ResponseEntity.ok(ApiRes.withSuccess("success"));
    }

    @DeleteMapping
    public ResponseEntity<ApiRes<String>> delete(@RequestBody UnitTrustFundWatchListDTO request) {

        unitTrustFundWatchListUseCase.removeFromWatchList(request);

        return ResponseEntity.ok(ApiRes.withSuccess("success"));
    }
}
