package com.jbtan.divit.rest;

import com.jbtan.divit.alerts.AlertService;
import com.jbtan.divit.alerts.TelegramAlertServiceImpl;
import com.jbtan.divit.auth.AuthResponse;
import com.jbtan.divit.auth.AuthService;
import com.jbtan.divit.constant.WebConstants;
import com.jbtan.divit.dto.SendAlertRequest;
import com.jbtan.divit.dto.SendAlertResponse;
import com.jbtan.divit.oauth2.google.GoogleLoginRequest;
import com.pengrad.telegrambot.response.SendResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(WebConstants.API_PREFIX + "test")
@Slf4j
@RequiredArgsConstructor
@CrossOrigin("*")
public class TestController {

    private final AlertService alertService;

    @GetMapping("/send-alert")
    public ResponseEntity<ApiRes<SendAlertResponse>> alert(@RequestParam String chatId) {

        return ResponseEntity.ok(ApiRes
            .withSuccess(alertService
                .sendAlert(SendAlertRequest.builder()
                    .message("Hello")
                    .chatId(chatId)
                    .build())));
    }
}
