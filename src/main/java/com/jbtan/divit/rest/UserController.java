package com.jbtan.divit.rest;

import com.jbtan.divit.constant.WebConstants;
import com.jbtan.divit.domain.User;
import com.jbtan.divit.service.UserService;
import com.jbtan.divit.useCase.UserUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(WebConstants.API_PREFIX)
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserUseCase userUseCase;

    @GetMapping("/users/info")
    public ResponseEntity<ApiRes<User>> getUserInfos() {
        return ResponseEntity.ok(ApiRes.withSuccess(userUseCase.getUserDetails()));
    }
}
