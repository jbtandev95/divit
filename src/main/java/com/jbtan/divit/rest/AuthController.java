package com.jbtan.divit.rest;

import com.jbtan.divit.auth.AuthResponse;
import com.jbtan.divit.auth.AuthService;
import com.jbtan.divit.constant.WebConstants;
import com.jbtan.divit.oauth2.google.GoogleLoginRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(WebConstants.OAUTH_API_PREFIX)
@Slf4j
@RequiredArgsConstructor
@CrossOrigin("*")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/google")
    public ResponseEntity<ApiRes<AuthResponse>> googleLogin(@RequestBody GoogleLoginRequest body) {
        return ResponseEntity.ok(ApiRes.withSuccess(authService.handleGoogleLogin(body)));
    }
}
