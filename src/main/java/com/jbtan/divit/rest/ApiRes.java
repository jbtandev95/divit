package com.jbtan.divit.rest;

import com.jbtan.divit.enums.ApiStatusCode;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder(toBuilder = true)
public class ApiRes<T> implements Serializable {

    private String code;

    private T data;

    public static <T> ApiRes<T> withSuccess(T data) {

        return ApiRes
                .<T>builder()
                .data(data)
                .code(ApiStatusCode.SUCCESS.getCode())
                .build();
    }

    public static <T> ApiRes<T> withStatusCode(ApiStatusCode statusCode, T data) {

        return ApiRes
                .<T>builder()
                .data(data)
                .code(statusCode.getCode())
                .build();
    }
}
