package com.jbtan.divit.rest;

import com.jbtan.divit.constant.WebConstants;
import com.jbtan.divit.domain.UnitTrustFund;
import com.jbtan.divit.service.UnitTrustFundService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(WebConstants.API_PREFIX)
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundController {

    private final UnitTrustFundService unitTrustFundService;

    @GetMapping("/funds")
    public ResponseEntity<ApiRes<List<UnitTrustFund>>> getFunds(@RequestParam Long providerId, Pageable pageable){
        log.info("auth {} ", SecurityContextHolder.getContext().getAuthentication());

        return ResponseEntity.ok(ApiRes.withSuccess(unitTrustFundService.findFundsByProviderId(providerId, pageable).getContent()));
    }

}
