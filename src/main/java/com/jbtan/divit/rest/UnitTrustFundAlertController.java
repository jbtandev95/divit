package com.jbtan.divit.rest;

import com.jbtan.divit.constant.WebConstants;
import com.jbtan.divit.domain.UnitTrustFund;
import com.jbtan.divit.domain.UnitTrustFundAlert;
import com.jbtan.divit.dto.UnitTrustFundAlertDTO;
import com.jbtan.divit.service.UnitTrustFundAlertService;
import com.jbtan.divit.useCase.UnitTrustFundAlertUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(WebConstants.API_PREFIX)
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundAlertController {

    private final UnitTrustFundAlertUseCase unitTrustFundAlertUseCase;

    @GetMapping("/alerts")
    public ResponseEntity<ApiRes<List<UnitTrustFundAlert>>> getAlertLists(Pageable pageable){
        return ResponseEntity.ok(ApiRes.withSuccess(unitTrustFundAlertUseCase.getList(pageable).getContent()));
    }

    @DeleteMapping("/alerts/{id}")
    public ResponseEntity<ApiRes<String>> deleteAlerts(@PathVariable Long id){

        if (id == null) {
            return ResponseEntity.noContent().build();
        }

        unitTrustFundAlertUseCase.deleteAlert(id);

        return ResponseEntity.ok(ApiRes.withSuccess("Completed"));
    }

    @PostMapping("/alerts")
    public ResponseEntity<ApiRes<String>> createNewAlert(@RequestBody UnitTrustFundAlertDTO request){

        unitTrustFundAlertUseCase.createNewAlert(request);

        return ResponseEntity.ok(ApiRes.withSuccess("Completed"));
    }
}
