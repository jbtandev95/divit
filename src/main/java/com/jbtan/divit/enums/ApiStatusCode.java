package com.jbtan.divit.enums;

import lombok.Getter;

@Getter
public enum ApiStatusCode {
    SUCCESS("200"),FAIL("500"),IMMEDIATE_ASSIST("400");

    private final String code;

    ApiStatusCode(String code){
        this.code = code;
    }
}
