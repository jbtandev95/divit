package com.jbtan.divit.enums;

public enum Status {

    ACTIVE, INACTIVE, DELETED;
}
