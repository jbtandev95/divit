package com.jbtan.divit.enums;

public enum LoginProvider {
    LOCAL, GOOGLE, FACEBOOK, TELEGRAM, TWITTER
}
