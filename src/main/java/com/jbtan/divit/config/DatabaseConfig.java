package com.jbtan.divit.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@Configuration
@EntityScan(basePackages = "com.jbtan.divit")
public class DatabaseConfig {
}
