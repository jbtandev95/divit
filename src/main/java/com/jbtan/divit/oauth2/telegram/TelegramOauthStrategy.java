package com.jbtan.divit.oauth2.telegram;

import com.jbtan.divit.auth.AuthStrategy;
import com.jbtan.divit.dto.UserDTO;
import com.jbtan.divit.enums.LoginProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TelegramOauthStrategy implements AuthStrategy {

    @Override
    public LoginProvider getProvider() {
        return LoginProvider.TELEGRAM;
    }

    @Override
    public UserDTO getNewUser(String code, String redirectUri) {
        return null;
    }
}
