package com.jbtan.divit.oauth2.google;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.jbtan.divit.auth.AuthStrategy;
import com.jbtan.divit.dto.UserDTO;
import com.jbtan.divit.enums.LoginProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Service
@RequiredArgsConstructor
@Slf4j
public class GoogleOauthStrategy implements AuthStrategy {

    @Value("${oauth2.google.client-id}")
    private String CLIENT_ID;

    @Value("${oauth2.google.client-secret}")
    private String CLIENT_SECRET;

    public UserDTO getNewUser(String code, String redirectUri) {
        NetHttpTransport transport = new NetHttpTransport();
        GsonFactory gsonFactory = new GsonFactory();

        try {
            // authorize and get id token from google
            GoogleTokenResponse response = new GoogleAuthorizationCodeTokenRequest(
                transport,
                gsonFactory,
                CLIENT_ID,
                CLIENT_SECRET,
                code,
                redirectUri).execute();

            // retrieve google id token
            GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, gsonFactory)
                .setAudience(Collections.singletonList(CLIENT_ID))
                .build();
            GoogleIdToken googleIdToken = verifier.verify(response.getIdToken());

            if (googleIdToken != null) {
                GoogleIdToken.Payload payload = googleIdToken.getPayload();
                String email = payload.getEmail();
                String givenName = (String) payload.get("given_name");
                String familyName = (String) payload.get("family_name");
                String profileImageUrl = (String) payload.get("picture");
                // You can also extract other user information from the payload if needed

                return UserDTO
                    .builder()
                    .email(email)
                    .firstName(givenName)
                    .lastName(familyName)
                    .profileImageUrl(profileImageUrl)
                    .loginProvider(LoginProvider.GOOGLE)
                    .enable2fa(false)
                    .build();
            }
        } catch (IOException | GeneralSecurityException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    @Override
    public LoginProvider getProvider() {
        return LoginProvider.GOOGLE;
    }
}
