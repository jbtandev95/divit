package com.jbtan.divit.oauth2.google;

import lombok.Data;

public record GoogleLoginRequest(String code, String redirectUri) {
}
