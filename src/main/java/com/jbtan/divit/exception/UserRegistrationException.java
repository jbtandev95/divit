package com.jbtan.divit.exception;

import lombok.Getter;

@Getter
public class UserRegistrationException extends RuntimeException {

    public UserRegistrationException() {}

    public UserRegistrationException(String message) {
        super(message);
    }
}