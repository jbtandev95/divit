package com.jbtan.divit.exception;

import lombok.Getter;

@Getter
public class UserAlreadyExistsException extends RuntimeException {

    private String email;

    public UserAlreadyExistsException() {

    }

    public UserAlreadyExistsException(String message, String email) {
        super(message);
        this.email = email;
    }
}
