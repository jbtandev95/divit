package com.jbtan.divit.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;

import java.math.BigDecimal;

@Entity
@Data
@Table(name = "unit_trust_fund_alert")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UnitTrustFundAlert {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private BigDecimal priceAlert;

    @ManyToOne
    private UnitTrustFund fund;

    @ManyToOne
    private User user;
}
