package com.jbtan.divit.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Table(name = "unit_trust_fund")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UnitTrustFund {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fundName;

    private String fundAbbreviation;

    private BigDecimal fundPrice;

    @ManyToOne
    private UnitTrustFundProvider provider;
}
