package com.jbtan.divit.domain;

import com.jbtan.divit.enums.LoginProvider;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "user")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    @Column(name = "enable_2fa")
    private boolean enable2FA;

    @Enumerated(value = EnumType.STRING)
    private LoginProvider provider;

    private Long googleAccountId;

    private String profileImageUrl;

    public User id(Long id) {
        this.id = id;
        return this;
    }
}
