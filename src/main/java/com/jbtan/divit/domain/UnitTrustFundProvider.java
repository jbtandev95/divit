package com.jbtan.divit.domain;

import com.jbtan.divit.enums.Status;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "unit_trust_fund_provider")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UnitTrustFundProvider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String scrapeUrl;
}
