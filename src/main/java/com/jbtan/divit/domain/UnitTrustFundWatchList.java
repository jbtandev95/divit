package com.jbtan.divit.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;

import java.math.BigDecimal;

@Entity
@Table(name = "unit_trust_fund_watch_list")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UnitTrustFundWatchList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private UnitTrustFund fund;

    @ManyToOne
    private User user;
}
