package com.jbtan.divit.security;

import com.jbtan.divit.config.AuthProperties;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.time.Instant;
import java.util.Date;

import static com.jbtan.divit.constant.WebConstants.TOKEN_HEADER;
import static com.jbtan.divit.constant.WebConstants.TOKEN_PREFIX;

@Component
@Slf4j
public class TokenProvider {

    private final AuthProperties authProperties;

    private final JwtParser jwtParser;

    public TokenProvider(AuthProperties authProperties) {
        this.authProperties = authProperties;
        this.jwtParser = Jwts.parser().verifyWith(getSigningKey()).build();
    }


    public String createToken(Long id) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + authProperties.getExpiryInMilliSeconds());

        return Jwts.builder()
                .issuedAt(now)
                .expiration(expiryDate)
                .subject(id.toString())
                .signWith(getSigningKey())
                .compact();
    }

    private SecretKey getSigningKey() {
        byte[] keyBytes = authProperties.getTokenSecret().getBytes(StandardCharsets.UTF_8);
        return Keys.hmacShaKeyFor(Decoders.BASE64URL.decode(authProperties.getTokenSecret()));
//        return Keys.hmacShaKeyFor(keyBytes);
    }

    public boolean validateToken(String jwt) {
        Claims claims = jwtParser
            .parseSignedClaims(jwt).getPayload();

        return true;
    }

    public Claims resolveCLaims(HttpServletRequest req) {
        try {
            String token = resolveToken(req);
            if (token != null) {
                return parseJwtClaims(token);
            }
            return null;
        } catch (ExpiredJwtException ex) {
            req.setAttribute("expired", ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            req.setAttribute("invalid", ex.getMessage());
            throw ex;
        }
    }

    public Claims parseJwtClaims(String jwt) {
        return jwtParser.parseSignedClaims(jwt).getPayload();
    }

    public boolean validateClaims(Claims claims) throws AuthenticationException {
        try {
            return claims.getExpiration().after(new Date());
        } catch (Exception e) {
            throw e;
        }
    }

    public String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(TOKEN_HEADER);
        if (bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(TOKEN_PREFIX.length());
        }
        return null;
    }
}
