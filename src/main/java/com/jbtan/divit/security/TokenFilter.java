package com.jbtan.divit.security;

import io.jsonwebtoken.Claims;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;

@Component
@RequiredArgsConstructor
@Slf4j
public class TokenFilter extends OncePerRequestFilter {

    private final TokenProvider tokenProvider;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String jwt = null;
        try {
            jwt = tokenProvider.resolveToken(request);
            if (StringUtils.hasText(jwt)) {
                Claims claims = tokenProvider.resolveCLaims(request);

                if (claims != null && tokenProvider.validateClaims(claims)) {
                    Long userId = Long.parseLong(claims.getSubject());
                    populateAuthContext(userId);
                }
            }
        } catch (Exception ex) {
            response.setStatus(HttpStatus.FORBIDDEN.value());
        }

        filterChain.doFilter(request, response);
    }

    private void populateAuthContext(Long userId){
        Authentication authentication =
            new UsernamePasswordAuthenticationToken(userId,"",new ArrayList<>());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
