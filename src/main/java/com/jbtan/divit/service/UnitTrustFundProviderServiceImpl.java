package com.jbtan.divit.service;

import com.jbtan.divit.domain.UnitTrustFundProvider;
import com.jbtan.divit.repository.UnitTrustFundProviderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundProviderServiceImpl implements UnitTrustFundProviderService {

    private final UnitTrustFundProviderRepository unitTrustFundProviderRepository;

    @Override
    public List<UnitTrustFundProvider> retrieveAllProviders() {
        return unitTrustFundProviderRepository.retrieveAllActiveProviders();
    }
}
