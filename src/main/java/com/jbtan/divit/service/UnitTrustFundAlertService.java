package com.jbtan.divit.service;

import com.jbtan.divit.domain.UnitTrustFundAlert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UnitTrustFundAlertService {

    Page<UnitTrustFundAlert> getUnitTrustFundAlertService(Long userId, Pageable pageable);

    void saveUnitTrustFundAlert(UnitTrustFundAlert unitTrustFundAlert);

    void deleteUnitTrustFundAlert(Long id);
}
