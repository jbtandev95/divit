package com.jbtan.divit.service;

import com.jbtan.divit.domain.UnitTrustFundWatchList;
import com.jbtan.divit.dto.UnitTrustFundWatchListDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UnitTrustFundWatchListService {

    Page<UnitTrustFundWatchList> getWatchListByUserId(Long userId, Pageable pageable);

    void addToWatchList(Long userId, UnitTrustFundWatchListDTO request);

    void removeFromWatchList(Long userId, UnitTrustFundWatchListDTO request);
}
