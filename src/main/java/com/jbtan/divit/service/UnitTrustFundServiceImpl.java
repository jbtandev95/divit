package com.jbtan.divit.service;

import com.jbtan.divit.domain.UnitTrustFund;
import com.jbtan.divit.domain.UnitTrustFundProvider;
import com.jbtan.divit.dto.UnitTrustFundItem;
import com.jbtan.divit.repository.UnitTrustFundRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundServiceImpl implements UnitTrustFundService {

    private final UnitTrustFundRepository unitTrustFundRepository;

    @Override
    public Page<UnitTrustFund> findFundsByProviderId(Long providerId, Pageable pageable) {
        return unitTrustFundRepository.findFundsByProviderId(providerId, pageable);
    }

    @Override
    public void upsertFund(UnitTrustFundItem item) {
        Optional<UnitTrustFund> found = unitTrustFundRepository.findByFundAbbreviationAndProviderId(item.fundAbbreviation(), item.providerId());

        if (found.isPresent()) {
            UnitTrustFund foundFund = found.get();
            foundFund.setFundPrice(item.nav());

            unitTrustFundRepository.save(foundFund);
        }

        UnitTrustFund newFund = UnitTrustFund
                .builder()
                .fundName(item.fund())
                .fundAbbreviation(item.fundAbbreviation())
                .fundPrice(item.nav())
                .provider(UnitTrustFundProvider.builder().id(item.providerId()).build())
                .build();

        unitTrustFundRepository.save(newFund);
    }
}
