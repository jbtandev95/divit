package com.jbtan.divit.service;

import com.jbtan.divit.domain.User;
import com.jbtan.divit.dto.UserDTO;
import com.jbtan.divit.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User registerOAuthUser(UserDTO userDTO) {
        Optional<Long> userId = userRepository.findUserByEmail(userDTO.email());
        if (userId.isPresent()) {
//            throw new UserAlreadyExistsException("User email already exists!", userDTO.email());
            return User.builder().id(userId.get()).build();
        }
        User user = User.builder()
                .email(userDTO.email())
                .username(userDTO.username())
                .password(null)
                .firstName(userDTO.firstName())
                .lastName(userDTO.lastName())
                .provider(userDTO.loginProvider())
                .enable2FA(userDTO.enable2fa())
                .profileImageUrl(userDTO.profileImageUrl())
                .build();
        return userRepository.save(user);
    }

    public Optional<User> getUserDetails(Long id) {
        return userRepository.findById(id);
    }

}
