package com.jbtan.divit.service;

import com.jbtan.divit.domain.UnitTrustFundAlert;
import com.jbtan.divit.repository.UnitTrustFundAlertRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundAlertServiceImpl implements UnitTrustFundAlertService {

    private final UnitTrustFundAlertRepository unitTrustFundAlertRepository;

    @Override
    public Page<UnitTrustFundAlert> getUnitTrustFundAlertService(Long userId, Pageable pageable) {
        return unitTrustFundAlertRepository.getUnitTrustFundAlertService(userId, pageable);
    }

    @Override
    public void saveUnitTrustFundAlert(UnitTrustFundAlert unitTrustFundAlert) {
        unitTrustFundAlertRepository.save(unitTrustFundAlert);
    }

    @Override
    public void deleteUnitTrustFundAlert(Long id) {
        unitTrustFundAlertRepository.deleteById(id);
    }


}
