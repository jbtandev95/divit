package com.jbtan.divit.service;

import com.jbtan.divit.domain.UnitTrustFund;
import com.jbtan.divit.dto.UnitTrustFundItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UnitTrustFundService {

    Page<UnitTrustFund> findFundsByProviderId(Long providerId, Pageable pageable);

    void upsertFund(UnitTrustFundItem item);
}
