package com.jbtan.divit.service;

import com.jbtan.divit.domain.UnitTrustFund;
import com.jbtan.divit.domain.UnitTrustFundWatchList;
import com.jbtan.divit.domain.User;
import com.jbtan.divit.dto.UnitTrustFundWatchListDTO;
import com.jbtan.divit.repository.UnitTrustFundWatchListRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundWatchListServiceImpl implements UnitTrustFundWatchListService {

    private final UnitTrustFundWatchListRepository unitTrustFundWatchListRepository;

    @Override
    public Page<UnitTrustFundWatchList> getWatchListByUserId(Long userId, Pageable pageable) {
        return unitTrustFundWatchListRepository.getWatchListByUserId(userId, pageable);
    }

    @Override
    public void addToWatchList(Long userId, UnitTrustFundWatchListDTO request) {

        unitTrustFundWatchListRepository.save(UnitTrustFundWatchList.builder()
            .fund(UnitTrustFund.builder().id(request.fundId()).build())
            .user(User.builder().id(userId).build())
            .build());
    }

    @Override
    public void removeFromWatchList(Long userId, UnitTrustFundWatchListDTO request) {

        Optional<UnitTrustFundWatchList> opt =
            unitTrustFundWatchListRepository.findByIdAndUserId(request.id(), userId);

        opt.ifPresent(unitTrustFundWatchListRepository::delete);
    }
}
