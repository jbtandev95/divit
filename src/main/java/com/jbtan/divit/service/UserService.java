package com.jbtan.divit.service;

import com.jbtan.divit.domain.User;
import com.jbtan.divit.dto.UserDTO;

import java.util.Optional;

public interface UserService {

    User registerOAuthUser(UserDTO userDTO);

    Optional<User> getUserDetails(Long userId);
}
