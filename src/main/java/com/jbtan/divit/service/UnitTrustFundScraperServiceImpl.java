package com.jbtan.divit.service;

import com.jbtan.divit.dto.UnitTrustFundItem;
import com.jbtan.divit.factory.WebDriverFactory;
import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UnitTrustFundScraperServiceImpl implements UnitTrustFundScraperService {

    @Value("${webdriver.chrome.driver}")
    private String chromeDriverPath;

    private final UnitTrustFundProviderService unitTrustFundProviderService;

    private final UnitTrustFundService unitTrustFundService;

    @Override
    public void crawlAvailableFunds() {

        unitTrustFundProviderService.retrieveAllProviders().forEach(ups -> {
            WebDriver driver = WebDriverFactory.getChromeDriver();

            if (StringUtils.isBlank(ups.getScrapeUrl())) {
                return;
            }
            driver.get(ups.getScrapeUrl());

            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dnn_ctr1895_FPFundPrice_gwFund")));

            List<WebElement> fundList = element.findElements(By.tagName("tr"));

            fundList.forEach(fund -> {
                List<WebElement> cells = fund.findElements(By.tagName("td"));

                if (!cells.isEmpty()) {
                    UnitTrustFundItem item = UnitTrustFundItem
                            .builder()
                            .providerId(ups.getId())
                            .fund(cells.get(1).getText())
                            .fundAbbreviation(cells.get(2).getText())
                            .nav(new BigDecimal(cells.get(3).getText()))
                            .build();

                    unitTrustFundService.upsertFund(item);
                }
            });
            driver.quit();
        });
    }
}
