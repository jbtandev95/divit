package com.jbtan.divit.service;

import com.jbtan.divit.domain.UnitTrustFundProvider;

import java.util.List;

public interface UnitTrustFundProviderService {

    List<UnitTrustFundProvider> retrieveAllProviders();
}
