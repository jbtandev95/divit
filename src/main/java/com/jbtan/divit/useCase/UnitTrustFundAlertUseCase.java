package com.jbtan.divit.useCase;

import com.jbtan.divit.annotations.UseCase;
import com.jbtan.divit.domain.UnitTrustFundAlert;
import com.jbtan.divit.domain.User;
import com.jbtan.divit.dto.UnitTrustFundAlertDTO;
import com.jbtan.divit.mappers.UnitTrustFundAlertMapper;
import com.jbtan.divit.security.SecurityUtils;
import com.jbtan.divit.service.UnitTrustFundAlertService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@UseCase
@Service
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundAlertUseCase {

    private final UnitTrustFundAlertService unitTrustFundAlertService;

    public void createNewAlert(UnitTrustFundAlertDTO request) {

        Long userId = SecurityUtils.getCurrentUserId();

        if (request.id() != null) {
            return;
        }

        UnitTrustFundAlert fundAlert = UnitTrustFundAlertMapper.INSTANCE.toEntity(request);
        fundAlert.setUser(new User().id(userId));

        unitTrustFundAlertService.saveUnitTrustFundAlert(fundAlert);
    }

    public Page<UnitTrustFundAlert> getList(Pageable pageable) {

        Long userId = SecurityUtils.getCurrentUserId();

        return unitTrustFundAlertService.getUnitTrustFundAlertService(userId, pageable);
    }

    public void deleteAlert(Long id) {
        unitTrustFundAlertService.deleteUnitTrustFundAlert(id);
    }
}
