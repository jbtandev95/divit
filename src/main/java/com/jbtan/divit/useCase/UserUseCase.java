package com.jbtan.divit.useCase;

import com.jbtan.divit.annotations.UseCase;
import com.jbtan.divit.domain.User;
import com.jbtan.divit.security.SecurityUtils;
import com.jbtan.divit.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@UseCase
@Slf4j
@Service
public class UserUseCase {

    private final UserService userService;

    public User getUserDetails() {
        Long userId = SecurityUtils.getCurrentUserId();

        if (userId == null) {
            return null;
        }

        return userService.getUserDetails(userId).orElseThrow();
    }
}
