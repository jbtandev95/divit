package com.jbtan.divit.useCase;

import com.jbtan.divit.annotations.UseCase;
import com.jbtan.divit.domain.UnitTrustFundWatchList;
import com.jbtan.divit.dto.UnitTrustFundWatchListDTO;
import com.jbtan.divit.security.SecurityUtils;
import com.jbtan.divit.service.UnitTrustFundWatchListService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@UseCase
@Service
@RequiredArgsConstructor
@Slf4j
public class UnitTrustFundWatchListUseCase {

    private final UnitTrustFundWatchListService unitTrustFundWatchListService;

    public Page<UnitTrustFundWatchList> getWatchListByUserId(Pageable pageable) {

        Long userId = SecurityUtils.getCurrentUserId();

        return unitTrustFundWatchListService.getWatchListByUserId(userId, pageable);
    }

    public void addToWatchList(UnitTrustFundWatchListDTO request) {

        unitTrustFundWatchListService.addToWatchList(SecurityUtils.getCurrentUserId(), request);
    }

    public void removeFromWatchList(UnitTrustFundWatchListDTO request) {

        unitTrustFundWatchListService.removeFromWatchList(SecurityUtils.getCurrentUserId(), request);
    }
}
