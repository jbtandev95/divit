package com.jbtan.divit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DivitApplication {

	public static void main(String[] args) {
		SpringApplication.run(DivitApplication.class, args);
	}

}
