package com.jbtan.divit.dto;

import lombok.Builder;

import java.math.BigDecimal;

@Builder
public record UnitTrustFundItem(String fund, String fundAbbreviation, BigDecimal nav, Long providerId) {
}
