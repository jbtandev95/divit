package com.jbtan.divit.dto;

import com.jbtan.divit.enums.LoginProvider;
import lombok.Builder;

@Builder
public record UserDTO(String email, String username, String firstName, String lastName, boolean enable2fa,
                      String profileImageUrl, LoginProvider loginProvider) {
}
