package com.jbtan.divit.dto;

import lombok.Builder;

@Builder
public record UnitTrustFundWatchListDTO(Long id, Long fundId) {
}
