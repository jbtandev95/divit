package com.jbtan.divit.dto;

import java.math.BigDecimal;

public record UnitTrustFundAlertDTO(Long id, BigDecimal priceAlert, Long fundId) {
}
