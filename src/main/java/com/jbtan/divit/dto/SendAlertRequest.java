package com.jbtan.divit.dto;

import lombok.Builder;

@Builder
public record SendAlertRequest(String chatId, String message) {
}
