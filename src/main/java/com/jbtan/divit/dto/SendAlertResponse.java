package com.jbtan.divit.dto;

import lombok.Builder;

@Builder
public record SendAlertResponse(boolean status) {
}
