package com.jbtan.divit.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class WebConstants {
    public static final String API_PREFIX = "api/v1/";

    public static final String OAUTH_API_PREFIX = "api/v1/oauth/";

    public static final String TOKEN_HEADER = "Authorization";

    public static final String TOKEN_PREFIX = "Bearer ";
}
