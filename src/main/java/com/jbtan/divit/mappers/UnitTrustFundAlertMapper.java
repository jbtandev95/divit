package com.jbtan.divit.mappers;

import com.jbtan.divit.domain.UnitTrustFundAlert;
import com.jbtan.divit.dto.UnitTrustFundAlertDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UnitTrustFundAlertMapper {

    UnitTrustFundAlertMapper INSTANCE = Mappers.getMapper( UnitTrustFundAlertMapper.class );

    @Mapping(source = "fundId", target = "fund.id")
    UnitTrustFundAlert toEntity(UnitTrustFundAlertDTO dto);

    @Mapping(source = "fund.id", target = "fundId")
    UnitTrustFundAlertDTO toDto(UnitTrustFundAlert entity);
}
