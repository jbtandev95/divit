package com.jbtan.divit.alerts;

import com.jbtan.divit.dto.SendAlertRequest;
import com.jbtan.divit.dto.SendAlertResponse;

public interface AlertService {

    SendAlertResponse sendAlert(SendAlertRequest request);

}
