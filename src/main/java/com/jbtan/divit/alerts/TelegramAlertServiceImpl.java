package com.jbtan.divit.alerts;

import com.jbtan.divit.dto.SendAlertRequest;
import com.jbtan.divit.dto.SendAlertResponse;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TelegramAlertServiceImpl implements AlertService {

    private final TelegramBot telegramBot;

    @Override
    public SendAlertResponse sendAlert(SendAlertRequest request) {
        SendResponse response = telegramBot.execute(new SendMessage(request.chatId(), request.message()));

        return SendAlertResponse.builder().status(response.isOk()).build();
    }
}
