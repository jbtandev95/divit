package com.jbtan.divit.auth;

import com.jbtan.divit.dto.UserDTO;
import com.jbtan.divit.enums.LoginProvider;

public interface AuthStrategy {

    LoginProvider getProvider();

    UserDTO getNewUser(String code, String redirectUri);
}
