package com.jbtan.divit.auth;

import com.jbtan.divit.enums.LoginProvider;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthStrategyFactory {

    private final List<AuthStrategy> authStrategyList;

    private static final Map<LoginProvider, AuthStrategy> authStrategyMap = new HashMap<>();

    @PostConstruct
    public void initAuthServices() {
        for (AuthStrategy authStrategy : authStrategyList) {
            authStrategyMap.put(authStrategy.getProvider(), authStrategy);
        }
    }

    public static AuthStrategy getAuthStrategy(LoginProvider provider) {
        AuthStrategy authStrategy = authStrategyMap.get(provider);

        if (authStrategy == null) {
            throw new RuntimeException("No login service provider found");
        }

        return authStrategy;
    }

}
