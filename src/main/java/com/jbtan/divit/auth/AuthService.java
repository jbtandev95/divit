package com.jbtan.divit.auth;

import com.jbtan.divit.domain.User;
import com.jbtan.divit.dto.UserDTO;
import com.jbtan.divit.enums.LoginProvider;
import com.jbtan.divit.exception.UserRegistrationException;
import com.jbtan.divit.oauth2.google.GoogleLoginRequest;
import com.jbtan.divit.security.TokenProvider;
import com.jbtan.divit.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthService {

    private final UserService userService;

    private final TokenProvider tokenProvider;

    public AuthResponse handleGoogleLogin(GoogleLoginRequest googleLoginRequest) {
        UserDTO userDTO = AuthStrategyFactory
            .getAuthStrategy(LoginProvider.GOOGLE)
            .getNewUser(googleLoginRequest.code(), googleLoginRequest.redirectUri());

        if (userDTO == null) {
            throw new UserRegistrationException("User login / registration Failed!");
        }
        User user = userService.registerOAuthUser(userDTO);

        final Long userId = user.getId();

        return AuthResponse.builder()
                .userId(userId)
                .token(tokenProvider.createToken(userId))
                .build();
    }
}
