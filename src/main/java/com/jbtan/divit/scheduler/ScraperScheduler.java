package com.jbtan.divit.scheduler;

import com.jbtan.divit.service.UnitTrustFundProviderService;
import com.jbtan.divit.service.UnitTrustFundScraperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ScraperScheduler {

    private final UnitTrustFundScraperService unitTrustFundScraperService;

    @Scheduled(fixedRate = 30000)
    public void scrapeFunds() {
        log.info("scraping now...");
        unitTrustFundScraperService.crawlAvailableFunds();
    }
}
