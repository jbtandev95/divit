package com.jbtan.divit.repository;

import com.jbtan.divit.domain.UnitTrustFundProvider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UnitTrustFundProviderRepository extends JpaRepository<UnitTrustFundProvider, Long> {

    @Query("select utfp from UnitTrustFundProvider utfp " +
            "where utfp.status = 'ACTIVE' ")
    List<UnitTrustFundProvider> retrieveAllActiveProviders();
}
