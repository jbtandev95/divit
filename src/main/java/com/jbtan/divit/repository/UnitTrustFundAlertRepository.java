package com.jbtan.divit.repository;

import com.jbtan.divit.domain.UnitTrustFund;
import com.jbtan.divit.domain.UnitTrustFundAlert;
import com.jbtan.divit.domain.UnitTrustFundProvider;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UnitTrustFundAlertRepository extends JpaRepository<UnitTrustFundAlert, Long> {

    @Query("select utfa from UnitTrustFundAlert utfa " +
        "where utfa.user.id = :userId ")
    Page<UnitTrustFundAlert> getUnitTrustFundAlertService(@Param("userId") Long userId, Pageable pageable);
}
