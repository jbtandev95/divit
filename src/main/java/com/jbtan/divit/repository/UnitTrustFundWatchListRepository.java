package com.jbtan.divit.repository;

import com.jbtan.divit.domain.UnitTrustFundWatchList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UnitTrustFundWatchListRepository extends JpaRepository<UnitTrustFundWatchList, Long> {

    @Query("select utfwl from UnitTrustFundWatchList utfwl " +
        "where utfwl.user.id = :userId ")
    Page<UnitTrustFundWatchList> getWatchListByUserId(@Param("userId") Long userId, Pageable pageable);


    @Query("select utfwl from UnitTrustFundWatchList utfwl " +
        "where utfwl.user.id = :userId " +
        "and utfwl.id = :id ")
    Optional<UnitTrustFundWatchList> findByIdAndUserId(@Param("id") Long id, @Param("userId") Long userId);
}
