package com.jbtan.divit.repository;

import com.jbtan.divit.domain.UnitTrustFund;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UnitTrustFundRepository extends JpaRepository<UnitTrustFund, Long> {

    @Query("select utp from UnitTrustFund utp " +
            "where utp.fundAbbreviation = :fundAbbreviation " +
            "and utp.provider.id = :providerId ")
    Optional<UnitTrustFund> findByFundAbbreviationAndProviderId(@Param("fundAbbreviation") String fundAbbreviation, @Param("providerId") Long aLong);

    @Query("select utp from UnitTrustFund utp " +
            "where utp.provider.id = :providerId ")
    Page<UnitTrustFund> findFundsByProviderId(@Param("providerId") Long providerId, Pageable pageable);
}
