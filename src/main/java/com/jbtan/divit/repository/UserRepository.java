package com.jbtan.divit.repository;

import com.jbtan.divit.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select id from User where email = :email")
    Optional<Long> findUserByEmail(@Param("email") String email);
}
